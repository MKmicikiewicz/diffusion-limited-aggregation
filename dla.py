"""
vanilla DLA
"""


import numpy as np
from tqdm import tqdm
from PIL import Image
from fractal_dimension import analyse


def dla(init_grid, n_iters, n_walkers, n_iters_per_frame):
    num_frames = n_iters // n_iters_per_frame
    fractal_dimensions = []
    frames = np.zeros((num_frames,) + init_grid.shape)
    frames[0] = init_grid
    scene = init_grid

    walkers = np.zeros_like(frames)

    # scene = initial_scene.copy()
    num_x = init_grid.shape[0]
    num_y = init_grid.shape[1]

    walkers_x = np.random.randint(
        low=0,
        high=num_x - 1,
        size=(n_walkers,),
    )
    walkers_y = np.random.randint(
        low=0,
        high=num_y - 1,
        size=(n_walkers,),
    )

    walkers[0, walkers_x, walkers_y] = 1

    for i in tqdm(range(1, n_iters)):

        x_1 = np.clip(walkers_x - 1, 0, num_x - 1)
        x0 = walkers_x
        x1 = np.clip(walkers_x + 1, 0, num_x - 1)

        y_1 = np.clip(walkers_y - 1, 0, num_y - 1)
        y0 = walkers_y
        y1 = np.clip(walkers_y + 1, 0, num_y - 1)

        mask = False
        mask |= scene[x_1, y_1] != 0
        mask |= scene[x0, y_1] != 0
        mask |= scene[x1, y_1] != 0
        mask |= scene[x_1, y0] != 0
        mask |= scene[x0, y0] != 0
        mask |= scene[x1, y0] != 0
        mask |= scene[x_1, y1] != 0
        mask |= scene[x0, y1] != 0
        mask |= scene[x1, y1] != 0

        scene[walkers_x[mask], walkers_y[mask]] = 1

        new_count = np.count_nonzero(mask)
        walkers_x[mask] = np.random.randint(
            low=0,
            high=num_x - 1,
            size=new_count,
        )
        walkers_y[mask] = np.random.randint(
            low=0,
            high=num_y - 1,
            size=new_count,
        )

        is_step_x = np.random.randint(2, size=n_walkers).astype(bool) #TODO change random for distance to atractor
        is_step_positive = np.random.randint(2, size=n_walkers).astype(bool)

        walkers_x[is_step_x & is_step_positive] += 1
        walkers_x[is_step_x & ~is_step_positive] -= 1
        walkers_y[~is_step_x & is_step_positive] += 1
        walkers_y[~is_step_x & ~is_step_positive] -= 1

        walkers_x = np.clip(walkers_x, 0, num_x - 1)
        walkers_y = np.clip(walkers_y, 0, num_y - 1)

        if i % n_iters_per_frame == 0:
            frames[i // n_iters_per_frame] = scene
            walkers[i // n_iters_per_frame, walkers_x, walkers_y] = 1
            try:
                pil_image = Image.fromarray(np.uint8(frames[i//n_iters_per_frame]))
                fractal_dimensions.append(analyse(pil_image))
            except AssertionError:
                fractal_dimensions.append(0)



    return frames, walkers, fractal_dimensions